<?php 
define("NUM_H",3);
$apiKey = '8dbf43aabb7368799eb1dc763b754ece';
$cityId = $_GET['city'];
$openWeatherMapUrl = "http://api.openweathermap.org/data/2.5/forecast?id={$cityId}&appid={$apiKey}&units=metric&lang=it";

$ch = curl_init();

curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_URL, $openWeatherMapUrl);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
curl_setopt($ch, CURLOPT_VERBOSE, 0);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
$response = curl_exec($ch);

curl_close($ch);
$data = json_decode($response);
$currentTime = time();
$today = date("Y-m-d");

// 1 - Estrapolare le previsioni delle prossime tre ore del giorno corrente.
/*
for($i=0;$i<NUM_H;$i++){
    $wheater_date = date("Y-m-d",$data->list[$i]->dt);
    echo $wheater_date."<br/>";
    if($wheater_date != $today){
        break;
    }
    var_dump($data->list[$i]);
}*/

// 2 - Estrapolare la prima previsione dei giorni a seguire.
/*$date_ref = date("Y-m-d",strtotime($today .' +1 day'));
foreach($data->list as $item){
    $wheater_date = date("Y-m-d",$item->dt);
    if($date_ref == $wheater_date) {
        $date_ref = date("Y-m-d",strtotime($date_ref .' +1 day'));
        var_dump($item);
    }

}
*/

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css2?family=Material+Icons"
rel="stylesheet">
<link rel="stylesheet" href="./style/style.css">
    <title>Previsioni future</title>
</head>
<body>
    <div class="go-back-wrapper">
    <a href="/meteo" class="go-back">Torna Indietro</a>
    </div>
    <h1 class="title-group">Previsioni Giornaliere per le prossime 3 ore</h1>
    <?php
        for($i=0;$i<NUM_H;$i++){
            $wheater_date = date("Y-m-d",$data->list[$i]->dt);
            //echo $wheater_date."<br/>";
            if($wheater_date != $today){
                break;
            }
            //var_dump($data->list[$i]);
        
    ?>
    <div class="weather-card">
        <div>
            <img src="<?php echo "http://openweathermap.org/img/wn/{$data->list[$i]->weather[0]->icon}@2x.png" ?>" alt="<?php echo $data->list[$i]->weather[0]->description; ?>" title="<?php echo $data->list[$i]->weather[0]->description; ?>" />
            <div class="main-weather">
                <h1><?php //echo '$data->name'; ?></h1>
                <p><?php echo ucwords($data->list[$i]->weather[0]->description); ?></p>
                <p><?php echo date("d/m/Y H:i", $data->list[$i]->dt); ?></p>
            </div>
        </div>
        <div>
            <div class="temperature">
                <span><span class="material-icons temp-max">thermostat</span>Temp. Max: <?php echo $data->list[$i]->main->temp_max; ?>°C</span>
                <span><span class="material-icons temp-min">thermostat</span>Temp. Min: <?php echo $data->list[$i]->main->temp_min; ?>°C</span>
            </div>
            <div class="other-data">
                <span><span class="material-icons humidity">water_drop</span>Umidità: <?php echo $data->list[$i]->main->humidity; ?> %</span>
                <span><span class="material-icons wind">air</span>Vento: <?php echo $data->list[$i]->wind->speed; ?> km/h</span>
            </div>
        </div>
    </div>
    <?php } ?>

    <hr/>
    <br/>
    <h1 class="title-group">Prima Previsionie del Giorno per i prossimi 5 giorni</h1>
    <?php
        $date_ref = date("Y-m-d",strtotime($today .' +1 day'));
        foreach($data->list as $item){
            $wheater_date = date("Y-m-d",$item->dt);
            if($date_ref == $wheater_date) {
                $date_ref = date("Y-m-d",strtotime($date_ref .' +1 day'));
                //var_dump($item);
            
        
    ?>
    <div class="weather-card">
        <div>
            <img src="<?php echo "http://openweathermap.org/img/wn/{$item->weather[0]->icon}@2x.png" ?>" alt="<?php echo $item->weather[0]->description; ?>" title="<?php echo $item->weather[0]->description; ?>" />
            <div class="main-weather">
                <h1><?php //echo '$data->name'; ?></h1>
                <p><?php echo ucwords($item->weather[0]->description); ?></p>
                <p><?php echo date("d/m/Y H:i", $item->dt); ?></p>
            </div>
        </div>
        <div>
            <div class="temperature">
                <span><span class="material-icons temp-max">thermostat</span>Temp. Max: <?php echo $item->main->temp_max; ?>°C</span>
                <span><span class="material-icons temp-min">thermostat</span>Temp. Min: <?php echo $item->main->temp_min; ?>°C</span>
            </div>
            <div class="other-data">
                <span><span class="material-icons humidity">water_drop</span>Umidità: <?php echo $item->main->humidity; ?> %</span>
                <span><span class="material-icons wind">air</span>Vento: <?php echo $item->wind->speed; ?> km/h</span>
            </div>
        </div>
    </div>
    <?php }} ?>


</body>
</html>