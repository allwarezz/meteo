<!DOCTYPE html>
<html lang="it">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style/style.css">
    <title>Meteo</title>
</head>
<body>
<?php 
$file_content = file_get_contents('./data/city.list.json', FILE_USE_INCLUDE_PATH);
//var_dump($cities);
$cities = json_decode($file_content);
//var_dump($cities);
?>
   <header>
    <h1>Previsone di oggi per:</h1>
   </header> 
   <main>
    <form action="meteo.php" method="GET">
        <select id="city" name="city" required>
           <?php 
                $index = 0;
                foreach($cities as $city ) {
                    if($index > 100) {
                        break;
                    }
                    printf("<option value='%s'>%s</option>", $city->id, $city->name);
                    $index++;
                }
           ?> 
        </select>
        
        <input type="submit" value="Mostra Meteo Attuale">
    </form>
    <br/>
    <form action="previsioni.php" method="GET">
        <select id="city" name="city" required>
           <?php 
                $index = 0;
                foreach($cities as $city ) {
                    if($index > 100) {
                        break;
                    }
                    printf("<option value='%s'>%s</option>", $city->id, $city->name);
                    $index++;
                }
           ?> 
        </select>
        
        <input type="submit" value="Mostra Previsioni Future">
    </form>
   </main>
   <footer>
    Developed 😀
   </footer>
</body>
</html>